//
//  ViewController.swift
//  ExploringMathematics
//
//  Created by Thor Laack-Veeder on 4/16/15.
//  Copyright (c) 2015 Thor Laack-Veeder. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //each IBOutlet corresponds to a button on the page
    @IBOutlet weak var splitTable: UITableView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var lapButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var smallTimerLabel: UILabel!
    @IBOutlet weak var largeTimerLabel: UILabel!
    //global variables that are updated in the following methods
    var lapCount = 0
    var splitArray:NSMutableArray = []
    var timeElapsed:Float = 0.00
    var splitTimeElapsed:Float = 0.00
    var timerRunning = false
    var splitTimerRunning = false
    var largeTimer = NSTimer()
    var splitTimer = NSTimer()
    let cellIdentifier = "splitTimeCell" //used in tableView later
    
    /*
    ** For the viewDidLoad, the stopButton and lapButton are hidden
    ** to mimick Apple's stopwatch app on the IPhone5S
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        self.splitTable?.delegate = self
        self.splitTable?.dataSource = self
        stopButton.hidden = true
        lapButton.hidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.splitArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        cell.textLabel?.text = "Split " + "\(indexPath.row + 1)" + ":         " + String(format: "%.2f", self.splitArray[indexPath.row] as! Float)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    /*
    ** handleResetButton stops timers, and sets timers to 0. splitArray is emptied to reduce array overhead
    ** and is used to clear the tableView on reload
    */
    
    @IBAction func handleResetButton(sender: UIButton) {
        largeTimer.invalidate()
        splitTimer.invalidate()
        timeElapsed = 0;
        splitTimeElapsed = 0;
        largeTimerLabel?.text = String(format: "%.2f", timeElapsed as Float)
        smallTimerLabel.text = "\(splitTimeElapsed)"
        splitArray.removeAllObjects()
        self.splitTable?.reloadData()
    }
    
    /*
    ** handleStartButton starts each timer, largeTimer and splitTimer respectively, and calls methods
    ** to update the timer values every millisecond until the stop button or lap button is pressed
    */
    @IBAction func handleStartButton(sender: UIButton) {
        hideButtonsForStartButtonPress()
        if timerRunning == false{
            largeTimer = NSTimer.scheduledTimerWithTimeInterval(1/100, target: self, selector: Selector("calculateTimeElapsed"), userInfo:nil, repeats: true)
            splitTimer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("calculateSplitTimeElapsed"), userInfo:nil, repeats: true)
            timerRunning = true
            splitTimerRunning = true
        }
    }
    
    /*
    ** handleStop stops all timers, which is called by the Stop UI button
    */
    @IBAction func handleStopButton(sender: UIButton) {
        if timerRunning == true{
            //stop the timer if it's running
            invalidateTimers()
            timerRunning = false
            hideButtonsForStopButtonPress()
            
        }
    }
    /*
    ** LapButton saves split times and updates the tableview by displaying each entry in the splitTimeArray
    */
    @IBAction func handleLapButton(sender: UIButton) {
        lapCount++
        if splitTimerRunning == true{
            self.splitArray.addObject(splitTimeElapsed)
            splitTimer.invalidate()
            splitTimeElapsed = 0
            smallTimerLabel.text = String(format: "%.2f", splitTimeElapsed as Float)
            splitTimerRunning = false
        }
        if splitTimerRunning == false{
            splitTimer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("calculateSplitTimeElapsed"), userInfo:nil, repeats: true)
            splitTimerRunning = true
        }
        self.splitTable?.reloadData()
    }
    
    func calculateSplitTimeElapsed(){
        splitTimeElapsed += 0.01
        smallTimerLabel.text = String(format: "%.2f", splitTimeElapsed as Float)
    }
    func calculateTimeElapsed(){
        if lapCount == 0{
            timeElapsed += 0.01
            largeTimerLabel?.text = String(format: "%.2f", timeElapsed as Float)
            smallTimerLabel.text = largeTimerLabel?.text
        }
        else{
            timeElapsed += 0.01
            largeTimerLabel?.text = String(format: "%.2f", timeElapsed as Float)
            smallTimerLabel.text = String(format: "%.2f", splitTimeElapsed as Float)
        }
        
    }
    
    func hideButtonsForStartButtonPress(){
        stopButton.hidden = false
        lapButton.hidden = false
        startButton.hidden = true
        resetButton.hidden = true
    }
    
    func hideButtonsForStopButtonPress(){
        startButton.hidden = false
        resetButton.hidden = false
        lapButton.hidden = true
        stopButton.hidden = true
    }
    
    func invalidateTimers(){
        largeTimer.invalidate()
        splitTimer.invalidate()
    }
    
    
}
