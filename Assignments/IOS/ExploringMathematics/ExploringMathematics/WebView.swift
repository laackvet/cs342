//
//  WebView.swift
//  ExploringMathematics
//
//  Created by Thor Laack-Veeder on 4/20/15.
//  Copyright (c) 2015 Thor Laack-Veeder. All rights reserved.
//

import UIKit

class WebViewController: UIViewController{
    
/*
Following code was produced after going through tutorials online
*/
    
    override func viewDidLoad(){
        super.viewDidLoad()
        let myWebView:UIWebView=UIWebView(frame: CGRectMake(0,0,UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height))
        myWebView.loadRequest(NSURLRequest(URL: NSURL(string: "http://www.wolframalpha.com")!))
        self.view.addSubview(myWebView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
