//
//  MIDINotesViewController.swift
//  ExploringMathematics
//
//  Created by Sarah LeBlanc on 4/16/15.
//  Copyright (c) 2015 Thor Laack-Veeder. All rights reserved.
//

import UIKit
import AVFoundation

class MIDINoteViewController: UIViewController {
    var playingG = AVAudioPlayer()
    var playingA = AVAudioPlayer()
    var playingB = AVAudioPlayer()
    var playingBB = AVAudioPlayer()
    var playingC = AVAudioPlayer()
    var playingCS = AVAudioPlayer()
    var playingD = AVAudioPlayer()
    var playingE = AVAudioPlayer()
    var playingEB = AVAudioPlayer()
    var playingF = AVAudioPlayer()
    var playingFS = AVAudioPlayer()
    var playingG2 = AVAudioPlayer()
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer {
        var path = NSBundle.mainBundle().pathForResource(file as String, ofType:type as String)
        var url = NSURL.fileURLWithPath(path!)
        
        var error: NSError?
        
        var audioPlayer:AVAudioPlayer?
        audioPlayer = AVAudioPlayer(contentsOfURL: url, error: &error)
        
        return audioPlayer!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playingG = self.setupAudioPlayerWithFile("g", type:"wav")
        playingA = self.setupAudioPlayerWithFile("a", type:"wav")
        playingB = self.setupAudioPlayerWithFile("b", type:"wav")
        playingBB = self.setupAudioPlayerWithFile("bb", type: "wav")
        playingC = self.setupAudioPlayerWithFile("c", type:"wav")
        playingCS = self.setupAudioPlayerWithFile("cs", type: "wav")
        playingD = self.setupAudioPlayerWithFile("d", type:"wav")
        playingE = self.setupAudioPlayerWithFile("e", type:"wav")
        playingEB = self.setupAudioPlayerWithFile("eb", type: "wav")
        playingF = self.setupAudioPlayerWithFile("f", type:"wav")
        playingFS = self.setupAudioPlayerWithFile("ff", type: "wav")
        playingG2 = self.setupAudioPlayerWithFile("gg", type:"wav")
        //Do additional stuff
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func handleGButton() {
        playingG.play()
    }
    
    @IBAction func handleAButton() {
        playingA.play()
    }
    
    @IBAction func handleBButton() {
        playingB.play()
    }
    
    @IBAction func handleBBButton() {
        playingBB.play()
    }
    
    @IBAction func handleCButton() {
        playingC.play()
    }
    
    @IBAction func handleCSButton() {
        playingCS.play()
    }
    
    @IBAction func handleDButton() {
        playingD.play()
    }
    
    @IBAction func handleEButton() {
        playingE.play()
    }
    
    @IBAction func handleEBButton() {
        playingEB.play()
    }
    
    @IBAction func handleFButton() {
        playingF.play()
    }
    
    @IBAction func handleFSButton() {
        playingFS.play()
    }
    @IBAction func handleG2Button() {
        playingG2.play()
    }
}

