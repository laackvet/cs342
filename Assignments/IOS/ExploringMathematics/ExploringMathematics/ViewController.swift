//
//  ViewController.swift
//  ExploringMathematics
//
//  Created by Thor Laack-Veeder on 4/16/15.
//  Copyright (c) 2015 Thor Laack-Veeder. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var optionsTableView: UITableView?

    let cellIdentifier = "OptionsMenu"
    let options = ["Stop Watch", "Mathematica", "MIDI Notes"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.optionsTableView?.delegate = self
        self.optionsTableView?.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.options[indexPath.row]
        
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let index = indexPath.row
        if (index == 0) {
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("calculator") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else if (index == 1) {
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("WebView") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else if (index == 2) {
            let mainStoryboard = UIStoryboard(name: "Main", bundle:NSBundle.mainBundle())
            let vc:UIViewController = mainStoryboard.instantiateViewControllerWithIdentifier("midi") as! UIViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
       
        
        
        
    }
}

