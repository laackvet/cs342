package com.laackvet.sound;


import android.app.ListActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.content.Intent;
import java.util.ArrayList;
import android.app.ActionBar;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);

        ArrayList<String> listItems = new ArrayList<String>();
        listItems.add("Be in the band");
        listItems.add("Create a light show");
        listItems.add("Play Party Song");

        ListDemoAdapter adapter = new ListDemoAdapter(this, R.layout.list_cell, listItems);
        ListView listView = (ListView)this.findViewById(android.R.id.list);
        listView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }
        if (id == R.id.action_about) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView parent, View v, int position, long id) {
        if (position == 0) {
            Intent intent = new Intent(this, BandActivity.class);
            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        } else if (position == 1) {
            Intent intent = new Intent(this, StrobeActivity.class);
            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }

        else if (position == 2){
            Intent intent = new Intent(this, WebActivity.class);
            startActivity(intent);

            overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        }
    }



}
