package com.laackvet.sound;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.MenuItem;
import android.view.Menu;
import android.widget.Toast;
import android.view.LayoutInflater;
import android.os.CountDownTimer;

/**
 * Created by Sarah LeBlanc and Thor Laack-Veeder
 *
 * Method creates a strobe light activity, which flashes a color when the user
 * clicks a color button.
 */
public class StrobeActivity extends ActionBarActivity {
    static CountDownTimer timer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_strobe);
    }

    public void flashBlue(View view) {
        showColor("blue", R.layout.blue);
    }
    public void flashRed(View view) {
        showColor("red", R.layout.red);
    }
    public void flashYellow(View view) {
        showColor("yellow", R.layout.yellow);
    }
    public void flashGreen(View view) {
        showColor("green", R.layout.green);
    }
    public void flashPurple(View view) {
        showColor("purple", R.layout.purple);
    }
    public void flashWhite(View view) {
        showColor("white", R.layout.white);
    }
    public void flashOrange(View view) {
        showColor("orange", R.layout.orange);
    }
    public void flashPink(View view) {
        showColor("pink", R.layout.pink);
    }
    public void flashTurquoise(View view) {
        showColor("turquoise", R.layout.turquoise);
    }


    /*
     * showColor takes in a color, and integer that corresponds to a file in our layout folder.
     * showColor returns nothing, and simply flashes a color to the screen using Android's Toast Object.
     */
    public void showColor(String color, int file) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Toast toast = new Toast(this);
        View view = null;
        view = inflater.inflate(file, null);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(view);

        //timer set for .2 seconds, which gives the flash illusion
        timer = new CountDownTimer(200, 10) {
            public void onTick(long millisUntilFinished) {
                toast.show();
            }

            public void onFinish() {
                toast.cancel();
            }
        }.start();
    }

}
