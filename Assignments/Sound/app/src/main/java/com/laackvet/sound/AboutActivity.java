package com.laackvet.sound;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by Sarah LeBlanc and Thor Laack-veeder on 4/6/15.
 */
public class AboutActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}

