package com.laackvet.sound;

import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by thorlaack-veeder and Sarah LeBlanc on 4/7/15.
 * BandActivity creates the buttons, and instantiates MediaPlayer objects
 * Music plays when buttons are pressed, which is called via an onClick method in activity_band.xml.
 */
public class BandActivity extends ActionBarActivity {
    private MediaPlayer a, a_minor, b, b_minor, c, c_minor, d, d_minor, e, e_minor, f, f_minor, g, g_minor, highHat, snare, bass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_band);
        a = MediaPlayer.create(this, R.raw.a_chord);
        a_minor = MediaPlayer.create(this, R.raw.a_minor);
        b = MediaPlayer.create(this, R.raw.b_chord);
        b_minor = MediaPlayer.create(this, R.raw.b_minor);
        c = MediaPlayer.create(this, R.raw.c);
        c_minor = MediaPlayer.create(this, R.raw.c_minor);
        d = MediaPlayer.create(this, R.raw.d_chord);
        d_minor = MediaPlayer.create(this, R.raw.d_minor);
        e = MediaPlayer.create(this, R.raw.e_chord);
        e_minor = MediaPlayer.create(this, R.raw.e_minor_chord);
        f = MediaPlayer.create(this, R.raw.f_chord);
        f_minor = MediaPlayer.create(this, R.raw.f_minor_chord);
        g = MediaPlayer.create(this, R.raw.g_chord);
        g_minor = MediaPlayer.create(this, R.raw.g_minor);
        highHat = MediaPlayer.create(this, R.raw.highhat);
        snare = MediaPlayer.create(this, R.raw.snaredrum);
        bass = MediaPlayer.create(this, R.raw.bass);
    }

    @Override
    protected void onPause(){
        super.onPause();
        a.release();

    }

    public void playSound(MediaPlayer note, int fileName){
        if(note.isPlaying()){
            note.release();
        }
        note = MediaPlayer.create(this, fileName);
        note.start();
    }

    public void play_a_chord(View view) { playSound(a, R.raw.a_chord); }

    public void play_aMinor_chord(View view) { playSound(a_minor, R.raw.a_minor); }

    public void play_b_chord(View view) { playSound(b, R.raw.b_chord); }

    public void play_bMinor_chord(View view) { playSound(b_minor, R.raw.b_minor); }

    public void play_C_Chord(View view) { playSound(c, R.raw.c); }

    public void play_Cm_Chord(View view) { playSound(c_minor, R.raw.c_minor); }

    public void play_D_Chord(View view) { playSound(d, R.raw.d_chord); }

    public void play_D_Minor_Chord(View view) { playSound(d_minor, R.raw.d_minor); }

    public void play_Em_Chord(View view) { playSound(e_minor, R.raw.e_minor_chord); }

    public void play_E_Chord(View view) { playSound(e, R.raw.e_chord); }

    public void play_F_Minor_Chord(View view) { playSound(f_minor, R.raw.f_minor_chord); }

    public void play_F_Chord(View view) { playSound(f, R.raw.f_chord); }

    public void play_G_Minor_Chord(View view) { playSound(g_minor, R.raw.g_minor); }

    public void play_G_Chord(View view) { playSound(g, R.raw.g_chord); }

    public void play_Bass_Drum(View view){ playSound(bass, R.raw.bass); }

    public void play_high_Hat(View view){ playSound(highHat, R.raw.highhat); }

    public void play_Snare(View view) { playSound(snare, R.raw.snaredrum); }
}
