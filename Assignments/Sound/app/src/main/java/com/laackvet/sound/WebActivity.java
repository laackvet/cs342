package com.laackvet.sound;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by thorlaack-veeder on 4/7/15.
 *
 * Note: WebActivity does not close once you click the back button on our action bar.
 * However, it will stop shortly after WebActivity loses focus.
 * Didn't have time to resolve this issue completely, but since it was only a 2 second issue, I
 * thought that this was due to the emulator, not the code.
 */
public class WebActivity extends ActionBarActivity {

    private WebView appWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        appWebView = (WebView) findViewById(R.id.appWebView);
        appWebView.getSettings().setJavaScriptEnabled(true);
        appWebView.loadUrl("https://www.youtube.com/watch?v=AzRyxGBGiAE");
        appWebView.setWebViewClient(new WebActivityClient());
    }

    private class WebActivityClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView webview, String url){
            webview.loadUrl(url);
            return true;
        }


    }




}

