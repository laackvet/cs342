In the subdirectory
Ideas for app:
	GPS location by using accelerometer in phone
	Remote database access
	Login/authentication system
	Make things look good
	Notifications
	Sharing on social networks
	Camera
	On-device data storage
	Connecting to other phones, network access
	Inter-app communication
	Speech-to-text
	Security
	Handling gestures
	Info about your device
	Autosave on minimize
	Accessing External APIs
	Drawing
	Web views
	Images and Video
	Sound
	Multithreading

Event Loops:
	Initialize your program
	Create event queue
	While not done:
		Event = queue.getNextEvent() //might block
		processEvent(event)
	Shut down cleanup

Activity life-cycle:
	
